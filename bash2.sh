#!/usr/bin/env bash
#SBATCH --job-name=ppo20#SBATCH --output=ppo20%j.log
#SBATCH --error=ppo20%j.err
#SBATCH --mail-user=alhermi@uni-hildesheim.de
#SBATCH --partition=STUD
#SBATCH --gres=gpu:1

#!/usr/bin/env bash

DEVICES="0"

CUDA_VISIBLE_DEVICES="$DEVICES" python ppo20.py



