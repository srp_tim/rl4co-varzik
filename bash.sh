#!/usr/bin/env bash
#SBATCH --job-name=symnco20#SBATCH --output=symnco20%j.log
#SBATCH --error=symno20%j.err
#SBATCH --mail-user=alhermi@uni-hildesheim.de
#SBATCH --partition=STUD
#SBATCH --gres=gpu:1

#!/usr/bin/env bash

DEVICES="0"

CUDA_VISIBLE_DEVICES="$DEVICES" python run.py --model symnco --env tsp --env.num_loc 20 --trainer.max_epochs 3 --model.train_file "rl4co\data\data\tsp\tsp20_train_gen_seed1111.npz" --model.val_file "rl4co\data\data\tsp\tsp20_val_seed4321.npz" --model.test_file "rl4co\data\data\tsp\tsp20_test_seed1234.npz" --model.batch_size 128 --wandb_mode offline


