#!/usr/bin/env bash
#SBATCH --job-name=pomo20mpnn
#SBATCH --output=pomo20mpnn%j.log
#SBATCH --error=pomo20mpnn%j.err
#SBATCH --mail-user=alhermi@uni-hildesheim.de
#SBATCH --partition=STUD
#SBATCH --gres=gpu:1

#!/usr/bin/env bash

DEVICES="0"

CUDA_VISIBLE_DEVICES="$DEVICES" python pomo20_mlp.py



