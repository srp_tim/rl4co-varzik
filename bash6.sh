#!/usr/bin/env bash
#SBATCH --job-name=attn20#SBATCH --output=attn20%j.log
#SBATCH --error=attn20%j.err
#SBATCH --mail-user=alhermi@uni-hildesheim.de
#SBATCH --partition=STUD
#SBATCH --gres=gpu:1

#!/usr/bin/env bash

DEVICES="0"

CUDA_VISIBLE_DEVICES="$DEVICES" python run1.py



