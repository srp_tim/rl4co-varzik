#!/usr/bin/env bash
#SBATCH --job-name=attn20mpnn
#SBATCH --output=attn20gmpnn%j.log
#SBATCH --error=attn20mpnn%j.err
#SBATCH --mail-user=alhermi@uni-hildesheim.de
#SBATCH --partition=STUD
#SBATCH --gres=gpu:1

#!/usr/bin/env bash


DEVICES="0"


wandb login 9815f3b0e8ce08b8878d8ca9a84bca3dd70f3978
wandb offline


CUDA_VISIBLE_DEVICES="$DEVICES" python attn20_mpnn.py



