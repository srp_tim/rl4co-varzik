from rl4co.envs.tsp import TSPEnv
from rl4co.models.zoo import POMO
from rl4co.utils.trainer import RL4COTrainer
import torch
from rl4co.models.nn.graph.gcn import GCNEncoder
from rl4co.models.nn.graph.mpnn import MessagePassingEncoder

from lightning.pytorch.loggers import WandbLogger
logger = WandbLogger(project="rl4co", name = "pomo_gcn")

gcn_encoder = GCNEncoder(
    env_name='tsp', 
    embedding_dim=128,
    num_nodes=20, 
    num_layers=3,
)

mpnn_encoder = MessagePassingEncoder(
    env_name='tsp', 
    embedding_dim=128,
    num_nodes=20, 
    num_layers=3,
)

# Environment, Model, and Lightning Module
env = TSPEnv(num_loc=20, train_file = "rl4co/data/data/tsp/tsp20_train_gen_seed1111.npz", test_file =  "rl4co/data/data/tsp/tsp20_test_seed1234.npz", val_file = "rl4co/data/data/tsp/tsp20_val_seed4321.npz")
model = POMO(env,
    		policy_kwargs={
        		'encoder': gcn_encoder # gcn_encoder or mpnn_encoder
		},
                optimizer_kwargs={'lr': 1e-4}
                )

# Greedy rollouts over untrained model
device = torch.device("cuda" if torch.cuda.is_available() else "cpu")
td_init = env.reset(batch_size=[3]).to(device)
model = model.to(device)
out = model(td_init, phase="test", decode_type="greedy", return_actions=True)

# Plotting
print(f"Tour lengths: {[f'{-r.item():.2f}' for r in out['reward']]}")
for td, actions in zip(td_init, out['actions'].cpu()):
    env.render(td, actions)

# Training

trainer = RL4COTrainer(
    max_epochs=100,
    accelerator="gpu", 
    logger = logger
)


# Fit the model
trainer.fit(model)

# Test the model
trainer.test(model)

# Greedy rollouts over trained model (same states as previous plot)
model = model.to(device)
out = model(td_init, phase="test", decode_type="greedy", return_actions=True)

# Plotting
print(f"Tour lengths: {[f'{-r.item():.2f}' for r in out['reward']]}")
for td, actions in zip(td_init, out['actions'].cpu()):
    env.render(td, actions)

